# Maketron

Compilation of Makefiles to facilitate building projects.

![logo](https://gitlab.com/datenn/maketron/raw/master/logo.png)

[![pipeline status](https://gitlab.com/datenn/maketron/badges/master/pipeline.svg)](https://gitlab.com/datenn/maketron/commits/master)
[![coverage report](https://gitlab.com/datenn/maketron/badges/master/coverage.svg)](https://gitlab.com/datenn/maketron/commits/master)


## Prerequisites

* Linux
* MacOS

## Installation

Create a `Makefile` at the root directory of your project and paste the following line:

```bash
SHELL := /bin/sh

-include $(shell curl -sSL -o Makefile.Maketron "http://bit.ly/maketron"; echo Makefile.Maketron)

```

Now you can run `make init` and initialize the project.

## Usage

```bash
Available targets:

  aws-nuke/install                    Install AWS Nuke
  aws-nuke/run                        Ensures that ALL resources are destroyed in AWS, I MEAN ALL RESOURCES, FOR REAL!
  aws-nuke/version                    Display AWS_NUKE version
  clean                               Clean maketron
  help                                Help screen
  help/all                            Display help for all targets
  help/short                          This help short screen
  init                                Init maketron
  template/aws-nuke                   Copy AWS Nuke configuration template
  template/gitignore                  Copy .gitignore template
  template/readme                     Copy .gitignore template
  terraform/apply                     Apply Terraform plan
  terraform/check                     Checks Terraform format and validate .tf files.
  terraform/clean                     Cleans terraform
  terraform/destroy                   Destroys resources created by Terraform
  terraform/get-modules               Ensure all modules can be fetched
  terraform/get-plugins               Ensure all plugins can be fetched
  terraform/init                      Ensure all plugins can be fetched
  terraform/install                   Install terraform
  terraform/lint                      Lint check Terraform
  terraform/plan                      Plans the project
  terraform/recreate                  Destroys and creates the project again
  terraform/upgrade-modules           Upgrade all terraform module sources
  terraform/validate                  Basic terraform sanity check
  update                              Update maketron repository
```

## Running the tests

TODO: Setup CI/CD for this project on Gitlab

## Contributing
Please use [GitFlow][gitflow] to create your branches. And create a pull-request to merge them into `develop` or `master` branch.
Please make sure to update tests as appropriate.

## Authors and acknowledgment

* **Valter Silva** - [GitHub](http://github.com/valter-silva-au) [Gitlab](http://gitlab.com/valter.silva)

## Copyright

Copyright © datenn - All Rights Reserved

[maketron]: https://gitlab.com/datenn/maketron
[gitflow]: https://datasift.github.io/gitflow/IntroducingGitFlow.html
