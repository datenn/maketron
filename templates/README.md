# foobar

Foobar is a Python library for dealing with word pluralization.

![logo](https://gitlab.com/datenn/PROJECT_TITLE/raw/master/datenn.png)

[![pipeline status](https://gitlab.com/datenn/PROJECT_TITLE/badges/master/pipeline.svg)](https://gitlab.com/datenn/PROJECT_TITLE/commits/master)
[![coverage report](https://gitlab.com/datenn/PROJECT_TITLE/badges/master/coverage.svg)](https://gitlab.com/datenn/PROJECT_TITLE/commits/master)


## Prerequisites

You need to install ...

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install foobar.

```bash
pip install foobar
```

## Usage

```python
import foobar

foobar.pluralize('word') # returns 'words'
foobar.pluralize('goose') # returns 'geese'
foobar.singularize('phenomena') # returns 'phenomenon'
```

## Running the tests

To run the tests please run ...

## Contributing
Please use [GitFlow][gitflow] to create your branches. And create a pull-request to merge them into `develop` or `master` branch.
Please make sure to update tests as appropriate.

## Authors and acknowledgment

* **First Name** - [GitHub](http://) [Gitlab](http://)

## Copyright

Copyright © datenn - All Rights Reserved

[maketron]: https://gitlab.com/datenn/maketron
[gitflow]: https://datasift.github.io/gitflow/IntroducingGitFlow.html