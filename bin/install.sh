#!/usr/bin/env bash
export GITHUB_REPO="https://gitlab.com/${MAKETRON_ORG}/${MAKETRON_PROJECT}.git"

if [[ "${MAKETRON_PATH}" ]] && [[ -d "${MAKETRON_PATH}" ]]; then
  echo "Removing existing ${MAKETRON_PATH}"
  rm -rf "${MAKETRON_PATH}"
fi

echo "Cloning ${GITHUB_REPO}#${MAKETRON_BRANCH}..."
git clone -b ${MAKETRON_BRANCH} ${GITHUB_REPO} ".${MAKETRON_PROJECT}"